module.exports = [
	{
		type: "input",
		name: "SecretId",
		message: "用户的 SecretId",
		default: ""
	},
	{
		type: "input",
		name: "SecretKey",
		message: "用户的 SecretKey",
		default: ""
	},
	{
		type: "input",
		name: "Bucket",
		message: "存储桶的名称",
		default: ""
	},
	{
		type: "input",
		name: "Region",
		message: "存储桶所在地域",
		default: ""
	},
	{
		type: "input",
		name: "outDir",
		message: "存储桶中的目录",
		default: "/"
	}
];
