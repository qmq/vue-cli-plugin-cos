const path = require("path");

module.exports = (api, projectOptions) => {
	const utils = require("./utils")(projectOptions.pluginOptions.cos);
	api.registerCommand(
		"cos",
		{
			description: "将发布目录(dist)中的代码上传到腾讯云COS",
			usage: "vue-cli-service cos"
		},
		args => {
			let options = projectOptions.pluginOptions.cos;
			let outputDir = path.join(api.getCwd(), projectOptions.outputDir);

			var files = utils.mapDir(outputDir);
			utils.uploadFiles(files);
		}
	);
};
