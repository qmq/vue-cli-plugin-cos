const fs = require("fs");
const path = require("path");
var COS = require("cos-nodejs-sdk-v5");

class PluginCos {
	constructor(opts) {
		this.opts = opts;
	}

	get cos() {
		if (!this._cos) {
			this._cos = new COS({
				SecretId: this.opts.SecretId,
				SecretKey: this.opts.SecretKey
			});
		}
		return this._cos;
	}

	mapDir(dir, files = [], root = null) {
		root = root || dir;
		var temp = fs.readdirSync(dir);
		temp.forEach(filename => {
			let pathname = path.join(dir, filename);
			var stat = fs.statSync(pathname);
			if (stat.isDirectory()) {
				this.mapDir(pathname, files, root);
			} else {
				files.push({
					Bucket: this.opts.Bucket,
					Region: this.opts.Region,
					Key:
						this.opts.outDir +
						pathname.replace(root, "").replace(/\\/gi, "/"),
					FilePath: pathname
				});
			}
		});
		return files;
	}
	uploadFiles(files) {
		this.cos.uploadFiles(
			{
				files: files,
				SilceSize: 1024 * 1024,
				onProgress: function(info) {
					var percent = parseInt(info.percent * 10000) / 100;
					var speed =
						parseInt((info.speed / 1024 / 1024) * 100) / 100;
					console.log(
						"进度：" + percent + "%; 速度：" + speed + "Mb/s;"
					);
				},
				onFileFinish: function(err, data, options) {
					//console.log(options.Key + "上传" + (err ? "失败" : "完成"));
				}
			},
			function(err, data) {
				console.log("任务执行结果");
				if (err) {
					console.log(err);
					return;
				}
				data.files.forEach(item => {
					if (item.error) {
						console.error(
							item.options.Key,
							item.error || "上传成功"
						);
					} else {
						console.log(item.options.Key, item.error || "上传成功");
					}
				});
			}
		);
	}
}

module.exports = opts => new PluginCos(opts);
