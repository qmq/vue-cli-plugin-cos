const { log, error, openBrowser } = require("@vue/cli-shared-utils");

module.exports = api => {
	api.describeConfig({
		id: "com.tzpgame.com",
		name: "腾讯云COS",
		description: "将[dist]文件夹文件同步到腾讯云COS中",
		files: {
			vue: {
				js: ["vue.config.js"]
			}
		},
		onRead: ({ data, cwd }) => ({
			tabs: [
				{
					id: "tab1",
					label: "mtabs",
					prompts: [
						{
							type: "input",
							name: "SecretId",
							message: "用户的 SecretId",
							default: data.vue.pluginOptions.cos.SecretId
						},
						{
							type: "input",
							name: "SecretKey",
							message: "用户的 SecretKey",
							default: data.vue.pluginOptions.cos.SecretKey
						},
						{
							type: "input",
							name: "Bucket",
							message: "存储桶的名称",
							default: data.vue.pluginOptions.cos.Bucket
						},
						{
							type: "input",
							name: "Region",
							message: "存储桶所在地域",
							default: data.vue.pluginOptions.cos.Region
						},
						{
							type: "input",
							name: "outDir",
							message: "存储桶中的目录",
							default: data.vue.pluginOptions.cos.outDir || "/"
						}
					]
				}
			]
		}),
		onWrite: ({ prompts, answers, data, files, cwd, api }) => {
			let saveData = data.vue;
			saveData.pluginOptions.cos = Object.assign(
				data.vue.pluginOptions.cos,
				answers
			);
			api.assignData("vue", saveData);
		}
	});
};
