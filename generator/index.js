module.exports = (api, opts, rootOptions) => {
	api.extendPackage({
		scripts: {
			cos: "vue-cli-service cos"
		},
		vue: {
			pluginOptions: {
				cos: opts
			}
		}
	});
};
